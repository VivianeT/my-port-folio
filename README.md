# My Port-Folio

## What is it

This project is my professional port-folio made with HTML5/CSS3/JS (no framework) that can be seen live [here](https://www.vivianetixier.com).

This project is using [Slick library (JS Carousel, MIT license)](https://github.com/kenwheeler/slick)

[![Base of my port-folio](example.png)](https://www.vivianetixier.com)


## How to build

 - Just launch the `index.html` file to see the project running


## License

This project is under Proprietary license. This means you can't use this project without asking me.
